echo 'deploy app to server PROD: =======>'
rm -fr $HOME/pythonapp
mkdir -p $HOME/pythonapp
cd $HOME/pythonapp
git clone https://gitlab.com/dinhkkptit/pythonapp.git .
docker stack deploy --compose-file docker-compose-prod.yml --with-registry-auth stackpython
echo '=====> deploy success on PROD server'
